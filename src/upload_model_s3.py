#!/usr/bin/env python
#-*- coding: utf-8 -*-
import os

import boto3
from dotenv import load_dotenv
bucket_name = 'imk'


load_dotenv('.env')
# aws_access_key_id = os.getenv('aws_access_key_id')
# aws_secret_access_key = os.getenv('aws_secret_access_key')

def upload_model_s3():
    session = boto3.session.Session()
    s3 = session.client(
        service_name='s3',
        endpoint_url='https://storage.yandexcloud.net',
    )
    s3.upload_file('models/model_1.joblib', bucket_name, 'model_221703.joblib')
    # s3.upload_file('this_script.py', 'bucket-name', 'script/py_script.py')
    # Получить список объектов в бакете
    for key in s3.list_objects(Bucket=bucket_name)['Contents']:
        print(key['Key'])


upload_model_s3()




