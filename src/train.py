import click
from joblib import dump
import pandas as pd
from sklearn.linear_model import LinearRegression
from datetime import datetime
from numpy import round
import yaml


@click.command()
@click.option('--config', default='params/train.yaml')
def train(config):
    with open(config) as f:
        data_path, model_path, report_path = load_config(f)

        df = pd.read_csv(data_path)
        print(df.head())

        X = df['total_meters'].to_numpy().reshape(-1, 1)
        y = df['price']

        model = LinearRegression()
        model.fit(X, y)

        generate_train_report(X, df, model, report_path, y)

        dump(model, model_path)


def load_config(f):
    config = yaml.load(f, Loader=yaml.SafeLoader)
    data_path = config['data_path']
    model_path = config['model_path']
    report_path = config['report_path']
    return data_path, model_path, report_path


def generate_train_report(X, df, model, report_path, y):
    k = model.coef_
    b = model.intercept_
    r2 = model.score(X, y)
    print('Coef: ', model.coef_)
    report = [
        f'Time: {datetime.now()}\n',
        f'Training data len: {len(df)}\n',
        f'Formula: Price = {round(k)} * Area + {round(b)}\n',
        f'R2: {r2}\n'
    ]
    with open('reports/report_1.txt', 'w') as f:
        f.writelines(report)


if __name__== '__main__':
    train()